// import axios from 'axios'
import store from 'src/store'
import { S_Obj, L_Obj } from 'src/s_obj.js'

// import { getFoodKind,getFoodFreight,getFoodExpresst,getHeadergroup,getFoodSpec,calcuData,goCalcuCheckout_data,foodSrc,delItem, addItem,totalAmount,getVipAddr,setVipAddr,itmeQtyReload,goCheckout,goCheckout_kakar2,quickCheckout,goCalcuCheckout,toDecimal,finishCheckout,delVipAddr,getUnfinishorder } from 'src/libs/market.js'
// import { setVipAddr,delVipAddr } from 'src/libs/market.js'
import { toDecimal } from 'src/libs/market.js'
// import { showAlert,checkReceiver,CheckCompanyNo  } from 'src/libs/appHelper.js'
// import { showAlert,checkReceiver  } from 'src/libs/appHelper.js'
import { showImgSubsidiary  } from 'src/libs/appHelper.js'


	let this1 = {
		addrs: '',
		phone1: '',
		receiver: '',  //收件人
		uniform:'',
		p_PayChannel:'', //付款方式
	}

	/* 使用-前端記錄 */
	const o1 = L_Obj.read('CurShipData');
	if (o1) {
		// o1.p_PayChannel = '';
		this1 = o1;
	}
	GetSetPayChannel();


/* 
// let ParamVip1 = {
	// Nick_Name: '',
	// PreAddr: '',
	// PrePhone: '',
	// PreName: '',
	// PayChannel: '',
	// VendNo: '',
	// PreSpecLimit: '',
// };

const Obj1 = {
	vInfo1:{
		PreAddr: '',
		PrePhone: '',
		PreName: '',
		Nick_Name: '',
		PayChannel: '',
		VendNo: '',
		PreSpecLimit: '',
	},
	vInfo2:{				// 頁面暫存
		addrsTmp:'',
		mobileTmp:'',
		receiverTmp:'',
	},
	curAdd: { 		// 目前使用的shipData[]那一組
		GID: '',
		Addr: '',
		Phone: '',
		Nick_Name: '',
	},
	shipData:[],
	editShipData:'',

};

function validinfo() {
	const v2 = Obj1.vInfo2
		,errMsg = checkReceiver(v2);
console.log('validinfo-v2: ', v2);	// @@

	errMsg && showAlert(errMsg);
	return !errMsg;
}
function editVip(addrItem) {
	Obj1.editShipData = '01';

	const v2 = Obj1.vInfo2
	console.log('editVip-vInfo2: ', v2);	// @@
	console.log('editVip-addrItem: ', addrItem);	// @@

	v2.addrsTmp = addrItem.Addr;
	v2.mobileTmp = addrItem.Phone;
	v2.receiverTmp = addrItem.Nick_Name;

	// replaceIt.call(Obj1.vInfo2, addrItem);
}
function saveVip(p_index) {
	if (!validinfo()) { return; }

	Obj1.editShipData = '';

	const v2 		= Obj1.vInfo2
		,shipobj1 = Obj1.shipData[p_index]
	// console.log('okShipDate-shipobj1: ', shipobj1);	// @@

	const param2 = {
		GID: shipobj1["GID"],
		Addr: v2.addrsTmp,
		Phone: v2.mobileTmp,
		Nick_Name: v2.receiverTmp
	}

	setVipAddr(param2,()=>{
		replaceIt.call(shipobj1, param2);
		replaceIt.call(Obj1.curAdd, param2);

		// //if (Array.isArray(p_data)) store.state.mallData.shipData = p_data;
		// shipobj1["Addr"] = v2.addrsTmp;
		// shipobj1["Phone"] = v2.mobileTmp;
		// shipobj1["Nick_Name"] = v2.receiverTmp;
	})
}

function delVip(p_index) {
	Obj1.editShipData = '';
	delVipAddr({
		GID: Obj1.shipData[p_index]["GID"]
	}, () => {
		Obj1.shipData.splice(p_index, 1);
	});
}
function NewVipAddr() {
	if (!validinfo()) { return; }

	const v2 = Obj1.vInfo2
	const param2 = {
		GID: '',
		Addr: v2.addrsTmp,
		Phone: v2.mobileTmp,
		Nick_Name: v2.receiverTmp
	}

	setVipAddr(param2,(p_data)=>{
		if (Array.isArray(p_data)) {
			store.state.mallData.shipData = p_data;
			replaceIt.call(Obj1.curAdd, p_data);
		}
	});
}
function xxxx(addrItem) {
}

 */

function calAgioTotal(val1,val2) {
	return toDecimal(val1)+toDecimal(val2);
}

/* 優惠折抵 */
function calATAmount(v1) {
	let o1 = null;
	switch (v1) {
	case "c":
		o1 = this.chkOrder;
		break;
	case "t":
		o1 = this.tmpOrder;
		break;
	case "o":
		o1 = this.finishOrder;
		break;
	case "f":
		o1 = this.finishOrderTemp;
		break;
	// default:
	}

	if (!o1) { return 0; }

	// let ans1 = calAgioTotal(o1.AgioCost, o1.AgioTotal);
// console.log('優惠折抵-ans1,o1: ',ans1, o1);	// @@
	// return ans1;
	return calAgioTotal(o1.AgioCost, o1.AgioTotal)
}

function ShowPic_FI(mainID) {
	const it = store.state.mallData.foodInfo[mainID];
	return showImgSubsidiary(it ? it['PIC1'] : '');
}
function GetSetPayChannel(value) {
	const act1 = 'LstPayChannel'
	if (value) {
		S_Obj.save(act1, value);

	} else {
		let v1 = S_Obj.read(act1);
		if (v1) {
			this1.p_PayChannel = v1
		}
	}
}


/* 
function replaceIt(opts) {
	opts || (opts = {});
	// 參數取代
	for ( var i in opts ) { this[i] = opts[i]; }
}
function initMe() {
	for ( var x in this ) { this[x] = ''; }
}
 */





export {
	calAgioTotal, calATAmount, ShowPic_FI
	// Obj1,editVip,saveVip,delVip,NewVipAddr
	// ,replaceIt,initMe
	,this1,GetSetPayChannel
	// ,ReadJsonS,ReadJsonS,ReadJsonS,ReadJsonS,ReadJsonS,
}