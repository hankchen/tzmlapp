import { reactive } from '@vue/composition-api'
import { formatTime } from 'src/libs/timeHelper.js'


let Timer1 				= null
	,FlgTimer1			= false

const Clock1 = reactive({
	flag: false,
	// days: 0,
	// hours: 0,
	minutes: 0,
	seconds: 0,
	curtime: '',
})

/* 啟動定時器 */
function InitClock(timeInSecs) {
	const currentTime = Date.parse(new Date())
		,deadline 			= new Date(currentTime + timeInSecs*1000);

	const padzero = function(v1) {
		return ('0'+v1).slice(-2);
	}

  updateClock();

	/* 防止 setInterval 重複設置的問題 */
  if (Timer1 == null) { // 如果timer為空 則開啟定時器
		Timer1 = setInterval(updateClock, 1000);
		FlgTimer1 = true;
  }

  function updateClock() {
    const t = GetTimeRemaining(deadline);

    if (t.total <= 0) {
      clearInterval(Timer1);
			Timer1 = null;
			FlgTimer1 = false;
    }

		Clock1.flag = FlgTimer1;
		// Clock1.days = padzero(t.days);
		// Clock1.hours = padzero(t.hours);
		Clock1.minutes = padzero(t.minutes);
		Clock1.seconds = padzero(t.seconds);
		Clock1.curtime = formatTime(new Date(), 'HH:mm:ss');
		// console.log('updateClock-minutes, seconds: ', Clock1.minutes, Clock1.seconds);	// @@
  }
}

function GetTimeRemaining(endtime) {
  const total = Date.parse(endtime) - Date.parse(new Date())
		,divNum		= total / 1000
		,seconds 	= Math.floor(divNum % 60)
		,minutes 	= Math.floor((divNum / 60) % 60)
		// ,hours 		= Math.floor(divNum * 60 * 60) % 24
		// ,days 		= Math.floor(divNum * 60 * 60 * 24)

  return {
    total,
    // days,
    // hours,
    minutes,
    seconds
  };
}

export {InitClock, Clock1}
