/* 系統環境參數相關 */

// 依據 環境參數(測試站) or 網址(正式站) 判定要取用的 app 版本
// const getAppSite = () => {
//   // 如果是測試環境，依據 .env.local 來判定
//   const isDev = /localhost/.test(location.href)
//   if (isDev) return process.env.VUE_APP_SITE

//   // 如果是非測試站，依據網址判定
//   const isKakar = /kakar/.test(location.href)
//   if (isKakar) return 'kakar'
//   const isWuhulife = /LICHENapp/.test(location.href)
//   if (isWuhulife) return 'wuhulife'

//   // 預設： kakar
//   return 'wuhulife'
// }

// const appSite = getAppSite()

// // main App (kakar)
// import appLichen from 'src/appLichen.vue'
// import routerKakar from 'src/appLichen/router'

// // main App (wuhulife)
// import AppWuhulife from 'src/AppWuhulife.vue'
// import routerWuhulife from 'src/appWuhulife/router'

// // 依據 appSite 設定要顯示的 app & router
// let App, router
// switch(appSite) {
//   case 'kakar':
//     App = appLichen
//     router = routerKakar
//   break;
//   case 'wuhulife':
//     App = AppWuhulife
//     router = routerWuhulife
//   break;
// }

import app from 'src/App.vue'
import routerLichen from 'src/view/router'

let App = app
let router = routerLichen

export { App, router }
