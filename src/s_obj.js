/** 用Session Storage,暫存本機的東西 */
import axios from 'axios'
import store from 'src/store'
// import fetchData from 'src/libs/fetchData.js'

const EnterpriseID 	= '6082824697'
	,baseUrl 		= 'tzml.jh8.tw'
	,publicUrl		= `https://${baseUrl}/public/newsocket.ashx`

/* [SessionStorage] 緩存(HTML5的內建obj) */
let S_Obj = {
	mac:	 '',
	isWebProd:	 /web/.test(location.host),
	isStaging:	 /staging/.test(location.host),	// !isWebProd
	isDebug:	 /localhost/.test(location.host) || /^192.168/ig.test(location.host),
	isFromTest: function () {
		const v2 = store.state.userData.RtAPIUrl;
		v2 && v2.trim().toLowerCase();
		this.isDebug && console.log('isFromTest-RtAPIUrl: ', v2); // @@
		return /8012test/.test(v2);
	},
	isTestAPI(){
		// return this.isStaging && this.isFromTest();
		return this.isFromTest();
	},
	save(key, v1){
		sessionStorage[key] = (typeof(v1) === 'object') ? JSON.stringify(v1) : v1;
	},
	read(key){
		const value = sessionStorage[key]
		return /{|]/.test(value) ? JSON.parse(value) : value;
		// return (typeof(value) === 'string') ? JSON.parse(value) : value;
	},
	del(key){
		sessionStorage.removeItem(key);
	},
	// clear(){
		// sessionStorage.clear(); //刪除所有資料
	// },

};

/* [LocalStorage] 緩存(HTML5的內建obj) */
let L_Obj = {
	use: 'localStorage' in window && window.localStorage !== null,
	get1(v1){
		return typeof(v1) == 'object' ? JSON.stringify(v1) : v1;
	},
	get2(v1){
		return /{|]/.test(v1) ? JSON.parse(v1) : v1;
		// return (typeof(v1) === 'string') ? JSON.parse(v1) : v1;
	},
    save(key, v1){
		localStorage[key] = this.get1(v1);
	},
	read(key){
		const value = localStorage[key]
		return this.get2(value);
	},
	saveBa64(key, v1){		
		localStorage[key] = window.btoa(encodeURIComponent(this.get1(v1)));
	},
	readBa64(key){
		// const v1 = this.read(key);
		const v1 = localStorage[key]
		if (v1) {
			const v2 = decodeURIComponent(window.atob(v1))
			return this.get2(v2);

		} else {
			return null;
		}
	},
	del(key){
		localStorage.removeItem(key);
	},
	clear(){
		localStorage.clear();
	},
};


async function Init() {
	const body = {
		EnterpriseID,
		act: "GetMac",
	}
	const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
	let {data} = await axios.post(publicUrl, body, head)
	// => 存入mac
	if (data.Remark) {
		const res1 = JSON.parse(data.Remark)
		S_Obj.mac = res1.mac
		// console.log('存入mac-mac: ', S_Obj.mac); // @@
	}

	// const _txt = 'Init-uData: IsMobile: '+typeof(JSInterface)
				// +', LocalStorage 支援: '+L_Obj.use
	// alert(_txt);	// @@
}

setTimeout(function(){ Init(); }, 100);

export { S_Obj, L_Obj }
